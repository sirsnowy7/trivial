class Trivial
  @@meta = { version:"1.0.0", creator:"sirsnowy7", maintainer:"sirsnowy7" }
  def self.meta()
    return @@meta
  end
  
  def self.setup_tdb()
    print "How many questions? (3 to 50): "
    loop do
      ans = gets.to_i
      if ans > 50 or ans < 3
        print "Try again. "
        next
      else
        puts "Do you want to have all the categories?"
        puts "1. All play"
        puts "2. Pick category"
        loop do
          print ">> "
          anst = gets.to_i
          if anst == 1
            puts "Loading TriviaDB file.\n\n"
            self.load_tdb(ans)
            break
          elsif anst == 2
            cats = [ ["General Knowledge", 9],
              ["Entertainment: Books", 10],
              ["Entertainment: Film", 11],
              ["Entertainment: Music", 12],
              ["Entertainment: Musicals & Theatres", 13],
              ["Entertainment: Television", 14],
              ["Entertainment: Video Games", 15],
              ["Entertainment: Board Games", 16],
              ["Science and Nature", 17],
              ["Science: Computers", 18],
              ["Science: Mathematics", 19],
              ["Mythology", 20],
              ["Sports", 21],
              ["Geography", 22],
              ["History", 23],
              ["Politics", 24],
              ["Art", 25],
              ["Celebrities", 26],
              ["Animals", 27] ]
            i = 0
            cats.each do |cat|
              i += 1
              print i, ". ", cat[0], "\n"
            end
            loop do
              print ">> "
              ansr = gets.to_i + 8
              if ansr > 27 or ansr < 9
                print "Try again. "
                next
              else
                puts "Loading TriviaDB file.\n\n"
                self.load_tdb(ans, ansr)
                break
              end
            end
          end
          break
        end
      end
      break
    end
  end
  
  def self.load_tdb(amount=20,category="")
    if not category == ""
      category = "&category=#{category}"
    end
    begin
      addr = "https://opentdb.com/api.php?amount=#{amount}&type=multiple#{category}"
      tdb = open(addr)
      IO.copy_stream(tdb, "./questions/tdb.json")
    rescue
      puts "Could not download file..."
      return 1
    end
    file = File.open("questions/tdb.json", "r")
    content = file.read.force_encoding("ISO-8859-1")
    begin
      content = JSON.parse(content)
    rescue
      puts "Could not parse JSON."
      return 2
    end
    content = content["results"]
    content.each do |cont|
      cont["incorrect_answers"].push(cont["correct_answer"]).reverse!
      cont["answers"] = cont["incorrect_answers"]
      dels = ["correct_answer", "incorrect_answers", "category", "type", "difficulty"]
      dels.each do |del|
        cont.delete(del)
      end
    end
    file.close
    content.each do |x|
      if x["after"].nil?
        x["after"] = 0
      end
      Question.new(
        x["question"],
        x["answers"],
        x["after"] )
    end
  end
  
  def self.load_file(file)
    file = File.open(file, "r")
    content = file.read.force_encoding("ISO-8859-1")
    begin
      content = JSON.parse(content)
    rescue
      puts "Unable to parse this question file's JSON. Try another file."
      return false
    end
    self.load(content)
  end
  
  def self.load(content)
    content.each do |x|
      x = x[1]
      if x["after"].nil?
        x["after"] = 0
      end
      Question.new(
        x["question"],
        x["answers"],
        x["after"] )
    end
  end
  
  def self.config()
    puts "Sorry, no configuration is available, or needed, yet."
    return 0
  end
  
  def self.start()
    any_loaded = false
    loop do
      puts "How do you want to play Trivial today?"
      puts "1. Start the game"
      puts "2. Load a question pile"
      puts "3. Load many question piles"
      puts "4. Load the omnipile"
      puts "5. Load from TriviaDB"
      puts "6. Configure the game"
      puts "0. Exit\n\n"
      print ">> "
      ans = gets.to_i
      case ans
      when 0
        puts "Exiting. See you soon, I'd hope."
        exit 0
      when 1
        if any_loaded
          puts "Starting game with currently loaded piles."
          self.main()
        else
          puts "No loaded question piles. Try loading the default or some from TDB."
        end
      when 2
        begin
          print "Question pile to load: "
          self.load_file(gets.chomp)
        rescue
          puts "Problems opening pile..."
          next 
        end
        puts "Pile successfully loaded!"
        any_loaded = true
      when 3
        loop do
          begin
            print "Question pile to load (blank to stop opening piles): "
            file = gets.chomp
            if file == ""
              break
            else
              self.load_file(file)
            end
          rescue
            puts "Problems opening pile..."
            next
          end
          any_loaded = true
          puts "Pile opened successfully."
        end
      when 4
        self.load_file("questions/omnipile.json")
        puts "Omnipile loaded.\n\n"
        any_loaded = true
      when 5
        self.setup_tdb()
        any_loaded = true
      when 6
        self.config()
      else
        puts "That's not an option."
      end
    end
  end
  
  def self.init()
    player_a = loop do
      print "How many players? (1 to 10): "
      player_a = gets.to_i
      unless player_a <= 10 and player_a >= 1
        puts "Try again."
        next
      else
        puts "That's a decent amount."
        break player_a
      end
    end
    players = []
    player_a.times do |i|
      print "Enter Player #{i + 1}'s name: "
      name = gets.chomp
      players.push( { "name" => name, "score" => 0, "answer" => 0, "correct" => false } )
    end
    return players
  end
  
  def self.main()
    players = self.init()
    puts
    questions = Question.questions
    questions.shuffle!
    questions.each do |qst|
      puts qst.question
      t = 0
      answ = qst.answers.shuffle
      answ.each do |ans|
        t += 1
        print t, ". ", ans, "\n"
      end
      players.each do |player|
        print "\n", player["name"], ": ", player["score"], "\n"
        print "What is ", player["name"], "'s answer? "
        loop do
          player["answer"] = gets.to_i - 1
          if player["answer"] >= answ.length or player["answer"] < 0
            print "Try that again... "
            next
          else
            break
          end
        end
        right = answ.index(qst.answers[0])
        if player["answer"] == right
          player["correct"] = true
        else
          player["correct"] = false
        end
      end
      puts "", "Time for the results!", ""
      players.each do |player|
        if player["correct"]
          print player["name"], " earns 1 point! Good one.\n"
          player["score"] += 1
        else
          print "Sorry, but that was not correct, ", player["name"], ".\n"
        end
      end
      print "The correct answer was ", qst.answers[0], ".\n"
      puts "", qst.after, "" if qst.after != 0
      gets
    end
    players.sort_by! do |player|
      player["score"]
    end
    players.reverse!
    puts "Let's see how each of you did! Here's the ranks:"
    i = 0
    players.each do |player|
      i += 1
      print player["name"], " in rank ", i," with a score of ", player["score"], ".\n"
    end
    gets
    puts "Together, you've answered a total of #{Question.questions.length} questions. Good job, everyone!"
    puts "Alright, and that's a wrap, everybody! Hope you had a good time playing, and see you next time on Trivial!\n\n"
  end
end
