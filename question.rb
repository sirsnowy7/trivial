class Question
  @@questions = []
  def self.questions
    return @@questions
  end
  def self.questions=(new)
    @@questions = new
  end
  
  attr_accessor :question, :answers, :after
  def initialize(q, a, e)
    @question = q
    @answers = a
    @after = e
    @@questions.push(self)
  end
end
