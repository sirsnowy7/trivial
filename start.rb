require 'json'
require 'open-uri'
require_relative 'trivial'
require_relative 'question'

begin
  Trivial.start()
rescue Interrupt
  puts "Use the quit option next time."
end
